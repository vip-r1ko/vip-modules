# AntiFlash

Добавляет способность VIP-игрокам не получать ослепление от себя и/или союзников.


## Требования:

* [**Blind Hook**](https://hlmod.ru/resources/blind-hook.1161/)  последней версии
* [**[VIP] Core**](https://hlmod.ru/resources/vip-core.245/)  версии не ниже  **2.1.1 R**
* [**SourceMod**](https://www.sourcemod.net/downloads.php?branch=stable)  версии не ниже  **1.9**


## Поддерживаемые игры:
* CS: GO


## Параметры группы: 

```
"AntiFlash"    "режим"
```

Вместо `режим` писать:
* `1` - Нет ослепления от команды
* `2` - Нет ослепления от самого себя
* `3` - Нет ослепления от команды и от самого себя 
* `4` - Нет ослепления вообще


## Установка:

В  _**vip_modules.phrases.txt**_  добавить  

```
   "AntiFlash"
   {
       "ru"    "Анти флеш"
       "en"    "Anti Flash"
       "fi"    "Anti Flash"
   }
```
