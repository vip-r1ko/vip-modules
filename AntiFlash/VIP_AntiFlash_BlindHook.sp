#pragma semicolon 1

#include <sourcemod>
#include <blindhook>
#include <vip_core>

public Plugin:myinfo =
{
	name = "[VIP] AntiFlash (CS:GO)",
	author = "R1KO",
	version = "2.0"
};

#define	AF_FLAG_TEAM	(1<<0)	// 1
#define	AF_FLAG_SELF	(1<<1)	// 2
#define	AF_FLAG_FULL	(1<<2)	// 4

static const char g_szFeature[] = "AntiFlash";

public void OnPluginStart()
{
	if(VIP_IsVIPLoaded())
	{
		VIP_OnVIPLoaded();
	}
}

public void VIP_OnVIPLoaded()
{
	VIP_RegisterFeature(g_szFeature, INT);
}

public void OnPluginEnd()
{
	if(CanTestFeatures() && GetFeatureStatus(FeatureType_Native, "VIP_UnregisterFeature") == FeatureStatus_Available)
	{
		VIP_UnregisterFeature(g_szFeature);
	}
}

public Action CS_OnBlindPlayer(int iClient, int iAttacker, int iInflictor)
{
	if (VIP_IsClientFeatureUse(iClient, g_szFeature))
	{
		int iFlags = VIP_GetClientFeatureInt(iClient, g_szFeature);
		if (iFlags & AF_FLAG_FULL)
		{
			return Plugin_Stop;
		}
		if (iFlags & AF_FLAG_SELF && iAttacker == iClient)
		{
			return Plugin_Stop;
		}
		if (iFlags & AF_FLAG_TEAM && GetClientTeam(iClient) == GetEntProp(iInflictor, Prop_Data, "m_iTeamNum"))
		{
			return Plugin_Stop;
		}
	}

	return Plugin_Continue;
}