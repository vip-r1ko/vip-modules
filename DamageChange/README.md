# Damage Change

Позволяет изменять VIP-игрокам наносимый и получаемый урон.


## Требования:

* [**[VIP] Core**](https://hlmod.ru/resources/vip-core.245/)  версии не ниже  **2.1.1 R**
* [**SourceMod**](https://www.sourcemod.net/downloads.php?branch=stable)  версии не ниже  **1.9**


## Поддерживаемые игры:

* **CS:GO**
* **CS:S OB**
* **CS:S v34**


## Параметры группы: 

```
"CausedDamage"    "30" // На сколько процентов больше урона будет наносить игрок 
```

```
"ReceivedDamage"    "15" // На сколько процентов  меньше урона будет получать игрок
```


## Установка:

В  _**vip_modules.phrases.txt**_  добавить  

```
    "ReceivedDamage"
    {
        "ru"    "Получаемый урон"
        "en"    "Received damage"
        "fi"    "Aiheutuvat vahingot"
    }
    "CausedDamage"
    {
        "ru"    "Наносимый урон"
        "en"    "Caused damage"
        "fi"    "Aiheuttanut vahinkoa"
    }
```
