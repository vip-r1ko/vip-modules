#pragma semicolon 1
#include <sourcemod>
#include <sdkhooks>
#include <vip_core>

#pragma newdecls required

public Plugin myinfo =
{
	name = "[VIP] Damage Change",
	author = "R1KO",
	version = "1.3"
};

#define	RECEIVED_DAMAGE		0	// Получаемый урон
#define	CAUSED_DAMAGE		1	// Наносимый урон

static const char g_szFeature[][] = {"ReceivedDamage", "CausedDamage"};

float g_fClientDamage[MAXPLAYERS+1][2];

public void OnPluginStart()
{
	LoadTranslations("vip_modules.phrases");

	if(VIP_IsVIPLoaded())
	{
		VIP_OnVIPLoaded();
	}
}

public void VIP_OnVIPLoaded()
{
	VIP_RegisterFeature(g_szFeature[RECEIVED_DAMAGE], INT, _, OnToggleItem, OnDisplayItem);
	VIP_RegisterFeature(g_szFeature[CAUSED_DAMAGE], INT, _, OnToggleItem, OnDisplayItem);
}

public void OnPluginEnd() 
{
	if(CanTestFeatures() && GetFeatureStatus(FeatureType_Native, "VIP_UnregisterFeature") == FeatureStatus_Available)
	{
		VIP_UnregisterFeature(g_szFeature[RECEIVED_DAMAGE]);
		VIP_UnregisterFeature(g_szFeature[CAUSED_DAMAGE]);
	}
}

public void VIP_OnVIPClientLoaded(int iClient)
{
	if (VIP_IsClientFeatureUse(iClient, g_szFeature[RECEIVED_DAMAGE]))
	{
		g_fClientDamage[iClient][RECEIVED_DAMAGE] = GetReceivedClientDamage(iClient);
	}

	if (VIP_IsClientFeatureUse(iClient, g_szFeature[CAUSED_DAMAGE]))
	{
		g_fClientDamage[iClient][CAUSED_DAMAGE] = GetCausedClientDamage(iClient);
	}
}

public void OnClientPutInServer(int iClient) 
{
	g_fClientDamage[iClient][RECEIVED_DAMAGE]	=
	g_fClientDamage[iClient][CAUSED_DAMAGE]		= 1.0;

	SDKHook(iClient, SDKHook_OnTakeDamage, OnTakeDamage);
}

public Action OnToggleItem(int iClient, const char[] szFeature, VIP_ToggleState OldStatus, VIP_ToggleState &NewStatus)
{
	if(szFeature[0] == 'R')
	{
		g_fClientDamage[iClient][RECEIVED_DAMAGE] = NewStatus == ENABLED ? GetReceivedClientDamage(iClient) : 1.0;
		return Plugin_Continue;
	}

	g_fClientDamage[iClient][CAUSED_DAMAGE] = NewStatus == ENABLED ? GetCausedClientDamage(iClient): 1.0;

	return Plugin_Continue;
}

public bool OnDisplayItem(int iClient, const char[] szFeature, char[] szDisplay, int iMaxLen)
{
	if(VIP_IsClientFeatureUse(iClient, szFeature))
	{
		FormatEx(szDisplay, iMaxLen, "%T [%c %d %%]", szFeature, iClient, (szFeature[0] == 'R') ? '-':'+', VIP_GetClientFeatureInt(iClient, szFeature));
		return true;
	}

	return false;
}

public Action OnTakeDamage(int iClient, int &iAttaker, int &iInflictor, float &fDamage, int &iDamageType)
{
	static float fSourceDamage;
	fSourceDamage = fDamage;

	if(0 < iAttaker && iAttaker <= MaxClients && g_fClientDamage[iAttaker][CAUSED_DAMAGE] != 1.0)
	{
		fSourceDamage *= g_fClientDamage[iAttaker][CAUSED_DAMAGE];
	}

	if(g_fClientDamage[iClient][RECEIVED_DAMAGE] != 1.0)
	{
		fSourceDamage *= g_fClientDamage[iClient][RECEIVED_DAMAGE];
	}

	if(fSourceDamage != fDamage)
	{
		fDamage = fSourceDamage;
		return Plugin_Changed;
	}

	return Plugin_Continue;
}

float GetReceivedClientDamage(int iClient)
{
	return 1.0-float(VIP_GetClientFeatureInt(iClient, g_szFeature[RECEIVED_DAMAGE]))/100.0;
}

float GetCausedClientDamage(int iClient)
{
	return 1.0+float(VIP_GetClientFeatureInt(iClient, g_szFeature[CAUSED_DAMAGE]))/100.0;
}